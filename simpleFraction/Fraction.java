package simpleFraction;

public class Fraction
{
	private double num,denum;
	
	public Fraction(double num, double denum)
	{
		this.num = num;
		this.denum = denum;
	}
	
	public Fraction()
	{
		this(0, 0);
	}
	
	@Override
	public String toString()
	{
		return Double.valueOf(num).toString() + " " + Double.valueOf(denum).toString();
	}
}
